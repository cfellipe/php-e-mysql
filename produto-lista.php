<?php include 'cabecalho.php';
include 'conecta.php';
include 'banco-produto.php';?>

<table class="table table-striped table-bordered">
	<tr>
	
		<td><h4>Produtos</h4></td>
		<td><h4>Preço</h4></td>

	</tr>

	<?php  
	$produtos = listaProduto($conexao);

	foreach ($produtos as $produto ) :
		?>
		
		
		<tr>
			<td><?= $produto['nome'] ?></td>
			<td><?= $produto['preco'] ?></td>
		</tr>

		<?php  
	endforeach

	?>
</table>

<?php include 'rodape.php';?>
