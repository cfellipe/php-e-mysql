<?php
include 'cabecalho.php';
include 'conecta.php';
include 'banco-produto.php';

$nome    = $_GET["nome"];
$preco   = $_GET["preco"];

if (insereProduto($nome, $preco, $conexao)) {
?>
	<p class=" text-success">Produto <?= $nome?>, de R$: <?= $preco ?> adicionado com sucesso!</p>

<?php
}else {
    $msg = mysqli_error($conexao);//variavel coleta a mensagem de erro se houver

?> 
	<p class="text-danger">Produto não foi adicionado : <?= $msg ?></p>

<?php
}
?> 

		<a href="index.php"><button class="btn btn-success"> Pagina inicial</button></a>

<?php include 'rodape.php';?>